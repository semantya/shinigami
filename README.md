# Shell commands #

* kitchen
* virtctl

# Features #

* Hybrid API in Python for a hybrid cloud management (public / private / on-demand / backyard) with a VM kitchen.
* Provisionning of templates / machines using : API / Commndline / CloudFormation

# List of packages #

* Docker
* LXC containers
* UML (User-Mode Linux)
* VirtualBox
* Vagrant

### Cloud solutions : ###

* OpenStack
* Salt Stack

### Cloud providers : ###

* Amazon EC2
* OvH Hosting & APIs.