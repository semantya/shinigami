#!/bin/bash

echo '{{ fqdn }}' >/etc/hostname

echo "127.0.0.1	    localhost"                            >>/etc/hosts
echo "127.0.1.1	    {{ fqdn }}	{{ name }}"               >>/etc/hosts

echo "::1				    ip6-localhost ip6-loopback"   >>/etc/hosts
echo "fe00::0				ip6-localnet"                 >>/etc/hosts
echo "ff00::0				ip6-mcastprefix"              >>/etc/hosts
echo "ff02::0              ip6-allnodes"                 >>/etc/hosts
echo "ff02::2				ip6-allrouters"               >>/etc/hosts

hostname {{ fqdn }}

echo "nameserver 8.8.8.8"  > /etc/resolvconf/resolv.conf.d/head
echo "nameserver 8.8.4.4" >> /etc/resolvconf/resolv.conf.d/head

cp /etc/resolvconf/resolv.conf.d/head /etc/resolv.conf

{% include 'scripts/setup-before.sh' %}



{% include 'scripts/setup-after.sh' %}

