# -*- mode: ruby -*-
# vi: set ft=ruby :

{% include 'vagrant/providers/aws/inventory.rb' %}

Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu-trusty64"

{% include 'vagrant/providers/aws/config.rb' %}

{% for host in inventory %}
    config.vm.define "authority" do |{{ host.name }}|
        {{ host.name }}.vm.provision "shell", inline: "echo {{ host.fqdn }}>/etc/hostname"
		{{ host.name }}.vm.provision "shell", path: "usr/recipes/ubuntu.sh"

		{{ host.name }}.vm.provision "shell", path: "var/os/{{ host.os.name }}/setup-before.sh"
{% for recipe in host.recipes %}
		{{ host.name }}.vm.provision "shell", path: "usr/recipes/{{ recipe }}.sh"
{% endfor %}
		{{ host.name }}.vm.provision "shell", path: "var/os/{{ host.os.name }}/setup-after.sh"

{% if host.hardware.virtualbox %}
		{{ host.name }}.vm.provider "virtualbox" do |vb|
			vb.customize ["modifyvm", :id, "--memory", "{{ host.hardware.virtualbox.ram }}", "--cpus", "{{ host.hardware.virtualbox.cpu }}"]
		end
{% endif %}

{% if host.hardware.aws %}
        {{ host.name }}.vm.provider :aws do |aws, override|
{% if host.hardware.aws.instance_type %}            aws.instance_type = "{{ host.hardware.aws.instance_type }}"{% endif %}
{% if host.hardware.aws.region        %}            aws.region       = "{{ host.hardware.aws.region }}"{% endif %}
{% if host.hardware.aws.key_pair      %}            aws.keypair_name = "{{ host.hardware.aws.key_pair }}"{% endif %}

            aws.ami           = EC2_AMI[config.vm.box]
            {{ host.name }}.vm.box = "dummy"
		end
{% endif %}
    end
{% endfor %}

    {
        "web"    => { "image" => 'ubuntu-trusty64',   "ram" => 512,   "scale" => 0, "offset" => 100 },
        "hive"   => { "image" => 'oracle-64',         "ram" => 1024,  "scale" => 0, "offset" => 130 },
        "worker" => { "image" => 'ubuntu-trusty64',   "ram" => 512,   "scale" => 0, "offset" => 150 },
        "gae"    => { "image" => 'appscale',          "ram" => 4096,  "scale" => 0, "offset" => 180 },
        "stack"  => { "image" => 'openscale',         "ram" => 4096,  "scale" => 0, "offset" => 190 },
    }.each do |rb,opts|
		1.upto(opts["scale"]) do |ind|
			config.vm.define rb+"-"+ind.to_s do |node|
                #node.vm.name = "cluster_"+rb+"-"+ind.to_s
				node.vm.box = opts["image"]

				node.vm.network "private_network", ip: ("192.168.33."+(opts["offset"]+ind).to_s)

        		node.vm.provision "shell", inline: "echo "+rb+"-"+ind.to_s+".fleet.maher-pur.pl>/etc/hostname"
		        node.vm.provision "shell", path: "recipes/ubuntu.sh"
		        node.vm.provision "shell", path: "recipes/roles/"+rb+".sh"
				
				node.vm.provider "virtualbox" do |vb|
					vb.customize ["modifyvm", :id, "--memory", opts["ram"]]
				end
			end
		end
	end
end

