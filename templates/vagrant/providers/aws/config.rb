    config.vm.provider :aws do |aws, override|
        aws.access_key_id = "{{ aws.api_key }}"
        aws.secret_access_key = "{{ aws.api_secret }}"

        aws.region       = "{{ aws.region }}"

        aws.keypair_name = "{{ aws.key_pair }}"
        override.ssh.username = "ubuntu"
        override.ssh.private_key_path = "/orchestra/etc/ec2.pem"
    end

