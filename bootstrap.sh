#!/bin/bash

source /etc/lsb-release

if [[ ! -e $ASSEMBLY_DIR ]] ; then
    mkdir -p $ASSEMBLY_DIR
fi

target_soul=$TARGET_DIR"/catalog/"`hostname -f`
if [[ -f $target_soul ]] ; then
    source $target_soul
fi
unset target_soul

alias render_file='/cookbook/tools/render'

#ZOMBIE_DIR=`/home/mouad/Bureau/virt/vagrant/boxes`

#export SERVICEs="mysql postgresql" # -"$POSTGRES_VERSION

for key in $CB_CHARMs ; do
    export CHARM_NAME=$key

    export CHARM_PATH=/cookbook/charms/$CHARM_NAME

    export CHARM_CFG=$CHARM_PATH/templates

    if [[ -f $CHARM_PATH/env.sh ]] ; then
        source $CHARM_PATH/env.sh
    fi
done

unset CHARM_NAME CHARM_CFG

for key in $CB_CLUSTERs ; do
    #args=$(echo $key | tr "/" "\n")

    export CLUSTER_NAME=$key
    export CLUSTER_ROLE=master

    export CLUSTER_PATH=/cookbook/cluster/$CLUSTER_NAME

    export CLUSTER_CFG=$CLUSTER_PATH/templates

    if [[ -f $CLUSTER_PATH/env.sh ]] ; then
        source $CLUSTER_PATH/env.sh
    fi
done

unset CLUSTER_NAME CLUSTER_ROLE CLUSTER_PATH CLUSTER_CFG

for key in $CB_COMPONENTs ; do
    if [[ -f /cookbook/components/$key/env.sh ]] ; then
        export COMPONENT_NAME=$key
        export COMPONENT_CFG=/cookbook/components/$COMPONENT_NAME/templates

        source /cookbook/components/$COMPONENT_NAME/env.sh
    fi
done

unset COMPONENT_NAME COMPONENT_CFG

if [[ ! -f '/etc/environment.bak' ]] ; then
    mv /etc/environment /etc/environment.bak

    #ln -s /cookbook/env.sh /etc/environment
    echo "PATH=\"$PATH:$TARGET_DIR/usr/bin:$NEXUS_HOME\"" >>/etc/environment
fi

