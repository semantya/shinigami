#!/bin/bash

if [[ -x /usr/bin/supervisorctl ]] ; then
    supervisorctl reread
    supervisorctl update

    sleep 5

    supervisorctl status
fi

for svc in $DISABLE_SERVICEs ; do
    if [[ -x /etc/init.d/$svc ]] ; then
        service $svc stop

        update-rc.d -f $svc remove
    fi

    if [[ -f /etc/init/$svc.conf ]] ; then
        stop $svc
    fi
done

for svc in $ENABLE_SERVICEs ; do
    if [[ -x /etc/init.d/$svc ]] ; then
        service $svc restart

        update-rc.d -f $svc defaults
    fi

    if [[ -f /etc/init/$svc.conf ]] ; then
        restart $svc
    fi
done

