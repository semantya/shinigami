#!/bin/bash

cp $CHARM_CFG/collectd.conf /etc/collectd/collectd.conf

touch /etc/collectd/custom.conf

service collectd restart

rm -f /var/www/linfo

if [[ ! -e /var/www/linfo ]] ; then
    apt-get install -y --force-yes apache2 libapache2-mod-php5

    cp $CHARM_CFG/apache2-ports.conf   /etc/apache2/ports.conf
    cp $CHARM_CFG/apache2-default.conf /etc/apache2/sites-enabled/000-default

    ln -sf $CHARM_CFG/spanel /var/www/linfo

    usermod -a -G vagrant www-data

    service apache2 restart
fi

ufw allow in from $NET_DMZ to any port 8090 proto tcp

