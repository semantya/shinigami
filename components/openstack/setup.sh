#!/bin/bash

source /vagrant/env.sh

apt-get install -y dkms guestmount virtualbox libvirt-bin lxc lxctl kvm qemu-{kvm{,-extras},system,launcher,utils} bridge-utils openvswitch-switch openvswitch-datapath-dkms iptables dnsmasq tgt rabbitmq-server

if [[ ! -d /stack ]] ; then
    git clone $OPENSTACK_GIT_REPO /stack
fi

cd /stack

cp $COMPONENT_CFG/local.conf ./local.conf

/stack/tools/create-stack-user.sh

for grp in kvm libvirtd vde2-net vboxusers ; do
    usermoad -a -G $grp stack
done

chown stack:stack -R /stack

sudo -u stack -H -- ./unstack.sh
sudo -u stack -H -- ./stack.sh

cp /vagrant/etc/roles/farm/apache2/horizon.conf /etc/apache2/sites-enabled/horizon

service apache2 restart

ufw allow in 80/tcp
ufw allow in 443/tcp
ufw allow in 5000/tcp # keystone
ufw allow in 8004/tcp # heat
ufw allow in 8774/tcp # nova
ufw allow in 8776/tcp # cinder
ufw allow in 8777/tcp # ceilometer
ufw allow in 9292/tcp # glance
ufw allow in 9696/tcp # neutron

echo Please configure Gitlab by accessing 'http://{ip_address}/' using this credentials : admin / admin | python /vagrant/macro.py

