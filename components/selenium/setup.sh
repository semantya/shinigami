#!/bin/bash

if [[ ! -d $SELENIUM_HOME ]] ; then
    mkdir -p $SELENIUM_HOME
    useradd -d $SELENIUM_HOME -m -r selenium

    usermod -a -G devops selenium

    cp -af $COMPONENT_CFG/supervisor.conf /etc/supervisor/conf.d/selenium.conf
    #cp -af $COMPONENT_CFG/supervisor.conf /etc/supervisor/conf.d/
fi

#cp -f /vagrant/etc/roles/builder/selenium.config $SELENIUM_WORKSPACE/etc/selenium.config

ufw allow in from $NET_WAN to any port 4444 proto tcp

echo Please configure Gitlab by accessing : http://selenium.myopla-pur.pl/

