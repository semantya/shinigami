
#Install Graphite
apt-get install graphite-web graphite-carbon

#Configure a Database for Django
## Install PostgreSQL Components

apt-get install postgresql libpq-dev python-psycopg2

## Create a Database User and a Database
sudo -u postgres -H -- psql -c "CREATE USER graphite WITH PASSWORD 'graphite';"

sudo -u postgres -H -- psql -c "CREATE DATABASE graphite WITH OWNER graphit;"


#Configure the Graphite Web Application
cp $COMPONENT_CFG/local_settings.py /etc/graphite/local_settings.py

#Sync the Database
graphite-manage syncdb
