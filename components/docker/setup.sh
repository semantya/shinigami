#!/bin/bash

if [[ ! -f /usr/bin/docker.io ]] ; then

    apt-get -y install docker.io

    ln -sf /usr/bin/docker.io /usr/local/bin/docker
    sed -i '$acomplete -F _docker docker' /etc/bash_completion.d/docker.io

else 
    echo "Docker is already installed"
fi
