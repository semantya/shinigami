#* Checkout this link for further config : https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master *#

external_url 'http://code.myopla-pur.pl'

## https://gitlab.com/gitlab-org/gitlab-ce/blob/master/config/gitlab.yml.example#L118
gitlab_rails['ldap_enabled']   = true
gitlab_rails['ldap_method']    = 'plain' # 'ssl' or 'plain'
gitlab_rails['ldap_allow_username_or_email_login'] = true

gitlab_rails['ldap_host']      = 'reactor.myopla-pur.pl'
gitlab_rails['ldap_port']      = 389

gitlab_rails['ldap_base']      = 'CN=Users,DC=myopla-pur,DC=pl'
gitlab_rails['ldap_bind_dn']   = 'CN=gitlab gitlab,CN=Users,DC=myopla-pur,DC=pl'

##gitlab_rails['ldap_user_filter'] = '(&(objectCategory=person)(|(memberOf=CN=Dev,CN=Groups,DC=myopla-pur,DC=pl)(memberOf=CN=DevOps,CN=Groups,DC=myopla-pur,DC=pl)))'

gitlab_rails['ldap_uid']       = 'sAMAccountName'
gitlab_rails['ldap_password']  = 'gitlab'

git_data_dir "/myopla/repo/git-data"
