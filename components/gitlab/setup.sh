#!/bin/bash

if [[ ! -f /etc/gitlab/gitlab.rb ]] ; then 
    if [[ -f $ASSEMBLY_DIR/Ubuntu/$GITLAB_RELEASE.deb ]] ; then 
        dpkg -i $ASSEMBLY_DIR/Ubuntu/$GITLAB_RELEASE.deb
    fi
fi

if [[ -f /etc/gitlab/gitlab.rb ]] ; then 
   cp $COMPONENT_CFG/gitlab.rb /etc/gitlab/gitlab.rb

    gitlab-ctl reconfigure
    
    ufw allow in from $NET_WAN to any port 80/tcp
    ufw allow in from $NET_WAN to any port 443/tcp
    ufw allow in from $NET_WAN to any port 9418/tcp

    echo Please configure Gitlab by accessing 'http://192.168.11.251/' using this credentials : root / "5iveL!fe"
else 
    echo "Gitlab not installed !"
fi

