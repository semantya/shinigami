#!/bin/bash

if [[ ! -f /usr/share/jenkins/jenkins.war ]] ; then
    wget -q -O - http://pkg.jenkins-ci.org/debian-stable/jenkins-ci.org.key | sudo apt-key add -
    echo "deb http://pkg.jenkins-ci.org/debian-stable binary/"   >> /etc/apt/sources.list

    apt-get update

    apt-get --force-yes -y install jenkins

    usermod -a -G devops jenkins

    #update-rc.d -f jenkins remove

    #service jenkins restart
fi

if [[ -f /usr/share/jenkins/jenkins.war ]] ; then
    cp -af $COMPONENT_CFG/jenkins.conf /etc/default/jenkins

    #cp -af $COMPONENT_CFG/supervisor.conf /etc/supervisor/conf.d/jenkins.conf

    ufw allow in from $NET_DMZ to any port 8010 proto tcp
fi

