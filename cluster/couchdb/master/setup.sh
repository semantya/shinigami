#!/bin/bash

apt-get -y install couchdb

cp $CLUSTER_CFG/couchdb.conf /etc/couchdb/default.ini

ufw allow in from $NET_WAN to any port 5984 proto tcp

