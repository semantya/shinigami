#!/bin/bash

apt-get -y install redis-server  

cp $CLUSTER_CFG/redis.conf /etc/redis/redis.conf

ufw allow in 6379/udp
