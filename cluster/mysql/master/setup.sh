#!/bin/bash

exec_sql () {
    echo $* | mysql -u $MYSQL_USER --password="$MYSQL_PASSWORD"
}

echo "mysql-server mysql-server/root_password password $MYSQL_PASSWORD"       | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $MYSQL_PASSWORD" | debconf-set-selections

if [[ ! -f /usr/sbin/mysqld ]] ; then
    apt-get -y install mysql-server
fi

cp $CLUSTER_CFG/my.conf /etc/mysql/my.cnf

exec_sql "SET PASSWORD FOR 'root'@'%' = PASSWORD('$MYSQL_PASSWORD');"
exec_sql "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;"

ufw allow in from $NET_WAN to any port 3306 proto tcp

