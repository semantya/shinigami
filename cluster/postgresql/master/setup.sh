#!/bin/bash

if [[ ! -f /usr/bin/psql ]] ; then
    apt-get -y install unixodbc postgresql-$POSTGRES_VERSION{,-{pl{java-gcj,lua,r,sh},postgis}}

    cp $CLUSTER_CFG/postgresql.conf /etc/postgresql/$POSTGRES_VERSION/main/postgresql.conf
    cp $CLUSTER_CFG/pg_hba.conf     /etc/postgresql/$POSTGRES_VERSION/main/pg_hba.conf


    sudo -u postgres -H -- psql template1 -c "ALTER USER postgres with encrypted password '$POSTGRSQL_USER_PASSWORD';"

fi

ufw allow in from $NET_WAN to any port 5432 proto tcp

